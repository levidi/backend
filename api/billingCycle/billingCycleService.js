const billingCycle = require('./billingCycle');
const _ = require('lodash');

billingCycle.methods(['get', 'post', 'put', 'delete']);
billingCycle.updateOptions({new: true, runValidators: true});

billingCycle.after('post', sendErrorsOrNext).after('put', sendErrorsOrNext);

function sendErrorsOrNext(req, res, next){
  const bundle = res.locals.bundle;

  if(bundle.errors){
    var errors = parseErrors(bundle.errors);
    res.status(500).json({errors});//res.json({errors});
  }
  else
    next();
}

function parseErrors(nodeRestfulErrors){
  const errors = [];
  _.forIn(nodeRestfulErrors, error => errors.push(error.message));
  return errors;
}


billingCycle.route('count', (req, res, next) => {
  billingCycle.count((error, value) => {
    if(error)
      res.status(500).json({errros: [error]});//res.status(500).json({errors: [error]});
    else
      res.json({value});
  });
});

module.exports = billingCycle;
